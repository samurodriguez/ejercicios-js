// Ejercicio 3

// Una empresa quiere realizar concursos para promocionar sus productos, por lo que necesita ser capaz de extraer de una lista de participantes varios grupos de un número variable de miembros. Escribe una función que les permita escoger N grupos de M participantes cada uno, de forma que devuelva grupos aleatorios, donde un mismo participante no pueda formar parte de más de uno de los grupos ganadores (solo se puede ganar 1 vez).

// Puedes utilizar el siguiente array de participantes:

const participants = [
  {
    nick: "bigleopard803",
    name: "Hugo",
    surname: "Araújo",
    email: "hugo.araujo@example.com",
  },
  {
    nick: "yellowdog306",
    name: "Alice",
    surname: "Jean-Baptiste",
    email: "alice.jean-baptiste@example.com",
  },
  {
    nick: "beautifulbear293",
    name: "Pinja",
    surname: "Lammi",
    email: "pinja.lammi@example.com",
  },
  {
    nick: "ticklishbear115",
    name: "Bojan",
    surname: "Dumas",
    email: "bojan.dumas@example.com",
  },
  {
    nick: "organicmeercat397",
    name: "Eren",
    surname: "Ilıcalı",
    email: "eren.ilicali@example.com",
  },
  {
    nick: "organicdog481",
    name: "Emaús",
    surname: "das Neves",
    email: "emaus.dasneves@example.com",
  },
  {
    nick: "smallgorilla944",
    name: "Florence",
    surname: "Ross",
    email: "florence.ross@example.com",
  },
  {
    nick: "crazyduck454",
    name: "Lino",
    surname: "Pierre",
    email: "lino.pierre@example.com",
  },
  {
    nick: "bluefrog830",
    name: "Vilma",
    surname: "Heinonen",
    email: "vilma.heinonen@example.com",
  },
  {
    nick: "happyrabbit114",
    name: "Umut",
    surname: "Kılıççı",
    email: "umut.kilicci@example.com",
  },
];

// Ejemplo input: (participants, 2, 2)

// Ejemplo output:
// [
//   // Grupo 1
//   [
//     {
//       nick: "bluefrog830",
//       name: "Vilma",
//       surname: "Heinonen",
//       email: "vilma.heinonen@example.com",
//     },
//     {
//       nick: "smallgorilla944",
//       name: "Florence",
//       surname: "Ross",
//       email: "florence.ross@example.com",
//     },
//   ],
//   // Grupo 2
//   [
//     {
//       nick: "crazyduck454",
//       name: "Lino",
//       surname: "Pierre",
//       email: "lino.pierre@example.com",
//     },
//     {
//       nick: "yellowdog306",
//       name: "Alice",
//       surname: "Jean-Baptiste",
//       email: "alice.jean-baptiste@example.com",
//     },
//   ],
// ];
